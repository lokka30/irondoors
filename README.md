<div align="center">

<img src="docs/img/icon.png" alt="IronDoors Logo" width="100px">

# IronDoors

A very lightweight simple plugin to allow players to open iron doors and iron trap doors. This will also play the use animation and sound effect. Players can only open iron doors in places they can open wooden doors. This should work with all protection plugins.​

### Learn More and Download at [SpigotMC.org][1]

</div>

## License

    Copyright (C) 2019 - 2022 Ranull and contributors

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.


[1]: https://www.spigotmc.org/resources/irondoors.73022/
