package com.ranull.irondoors.interaction;

import com.ranull.irondoors.util.MaterialUtils;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.data.BlockData;
import org.bukkit.block.data.Openable;
import org.bukkit.block.data.type.Door;
import org.bukkit.block.data.type.TrapDoor;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

/**
 * Interaction handler for 1.13+ servers utilizing block data.
 *
 * @author Ranull
 */
public class BlockDataInteractionHandler implements InteractionHandler {

    @Override
    public boolean canInteract(
        final Player player,
        ItemStack itemStack,
        final Block block,
        final BlockFace blockFace
    ) {
        itemStack = itemStack == null ? new ItemStack(Material.AIR) : itemStack;
        Material newMaterial = null;
        Material material = block.getType();
        BlockData blockData = block.getBlockData().clone();

        //noinspection DuplicatedCode
        if (block.getType().toString().equals(MaterialUtils.IRON_DOOR_NAME)) {
            newMaterial = Material.getMaterial(MaterialUtils.OAK_DOOR_NAME);
        } else if (block.getType().toString().equals(MaterialUtils.IRON_TRAPDOOR_NAME)) {
            newMaterial = Material.getMaterial(MaterialUtils.OAK_TRAPDOOR_NAME);
        }

        if (newMaterial != null) {
            block.setType(newMaterial, false);
        }

        PlayerInteractEvent playerInteractEvent = new PlayerInteractEvent(player, Action.RIGHT_CLICK_BLOCK, itemStack, block, blockFace);

        Bukkit.getServer().getPluginManager().callEvent(playerInteractEvent);

        block.setType(material, false);
        block.setBlockData(blockData, false);

        return playerInteractEvent.useInteractedBlock() != Event.Result.DENY;
    }

    @Override
    public void toggleDoor(final Block block) {
        BlockData blockData = block.getBlockData();

        if (blockData instanceof Openable) {
            Openable openable = (Openable) blockData;

            openable.setOpen(!openable.isOpen());
            block.setBlockData(openable);

            if (blockData instanceof Door) {
                block.getWorld().playEffect(block.getLocation(), Effect.IRON_DOOR_TOGGLE, 0);
            } else if (blockData instanceof TrapDoor) {
                block.getWorld().playEffect(block.getLocation(), Effect.IRON_TRAPDOOR_TOGGLE, 0);
            }
        }
    }
}
