package com.ranull.irondoors.interaction;

import com.ranull.irondoors.util.MaterialUtils;
import com.ranull.irondoors.util.VersionUtils;
import org.bukkit.Bukkit;
import org.bukkit.Effect;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.block.BlockState;
import org.bukkit.entity.Player;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Door;
import org.bukkit.material.MaterialData;
import org.bukkit.material.Openable;
import org.bukkit.material.TrapDoor;

/**
 * Interaction handler for legacy (<1.13) servers based upon material data.
 *
 * @author Ranull
 */
public class MaterialDataInteractionHandler implements InteractionHandler {

    private static final boolean HAS_DOOR_EFFECTS = VersionUtils.v1_10_OR_NEWER;

    @Override
    public boolean canInteract(
        final Player player,
        ItemStack itemStack,
        final Block block,
        final BlockFace blockFace
    ) {
        itemStack = itemStack == null ? new ItemStack(Material.AIR) : itemStack;
        Material newMaterial = null;
        Material material = block.getType();
        BlockState blockState = block.getState();
        //noinspection deprecation
        MaterialData materialData = blockState.getData().clone();

        //noinspection DuplicatedCode
        if (block.getType().toString().equals(MaterialUtils.IRON_DOOR_NAME)) {
            newMaterial = Material.getMaterial(MaterialUtils.OAK_DOOR_NAME);
        } else if (block.getType().toString().equals(MaterialUtils.IRON_TRAPDOOR_NAME)) {
            newMaterial = Material.getMaterial(MaterialUtils.OAK_TRAPDOOR_NAME);
        }
        
        if (newMaterial != null) {
            block.setType(newMaterial, false);
        }

        PlayerInteractEvent playerInteractEvent = new PlayerInteractEvent(player, Action.RIGHT_CLICK_BLOCK, itemStack, block, blockFace);

        Bukkit.getServer().getPluginManager().callEvent(playerInteractEvent);

        block.setType(material, false);
        block.getState().setData(materialData);
        blockState.update(true, false);

        //noinspection deprecation
        return !playerInteractEvent.isCancelled();
    }

    @SuppressWarnings("deprecation")
    @Override
    public void toggleDoor(Block block) {
        if (block.getState().getData() instanceof Door && ((Door) block.getState().getData()).isTopHalf()) {
            block = block.getRelative(BlockFace.DOWN);
        }

        if (block.getState().getData() instanceof Openable) {
            BlockState blockState = block.getState();
            Openable openable = (Openable) blockState.getData();

            openable.setOpen(!openable.isOpen());
            blockState.setData((MaterialData) openable);
            blockState.update(true, true);

            if (HAS_DOOR_EFFECTS) {
                if (openable instanceof Door) {
                    block.getWorld().playEffect(block.getLocation(), Effect.IRON_DOOR_TOGGLE, 0);
                } else if (openable instanceof TrapDoor) {
                    block.getWorld().playEffect(block.getLocation(), Effect.IRON_TRAPDOOR_TOGGLE, 0);
                }
            } else {
                block.getWorld().playEffect(block.getLocation(), Effect.DOOR_TOGGLE, 0);
            }
        }
    }
}
