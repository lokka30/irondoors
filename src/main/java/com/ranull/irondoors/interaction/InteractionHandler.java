package com.ranull.irondoors.interaction;

import com.ranull.irondoors.util.VersionUtils;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

/**
 * Interface for interaction handlers, providing version-dependent logic for door interaction.
 *
 * @author Ranull
 */
public interface InteractionHandler {

    InteractionHandler INSTANCE = VersionUtils.v1_13_OR_NEWER ?
        new BlockDataInteractionHandler() : new MaterialDataInteractionHandler();

    boolean canInteract(
        final Player player,
        final ItemStack itemStack,
        final Block block,
        final BlockFace blockFace
    );

    void toggleDoor(final Block block);

}
