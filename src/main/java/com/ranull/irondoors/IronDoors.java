package com.ranull.irondoors;

import com.ranull.irondoors.listener.PlayerInteractListener;
import com.ranull.irondoors.util.VersionUtils;
import org.bstats.bukkit.Metrics;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Iron Doors plugin main class.
 *
 * @author Ranull
 */
public class IronDoors extends JavaPlugin {

    @Override
    public void onEnable() {
        if(!VersionUtils.v1_7_OR_NEWER) {
            getLogger().severe("Your server version '" + getServer().getVersion() +
                "' is not compatible with IronDoors.");
            Bukkit.getPluginManager().disablePlugin(this);
            return;
        }

        registerListeners();
        startMetrics();
    }

    private void registerListeners() {
        getServer().getPluginManager().registerEvents(new PlayerInteractListener(), this);
    }

    private void startMetrics() {
        new Metrics(this, 12870);
    }

}
