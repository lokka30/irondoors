package com.ranull.irondoors.listener;

import com.ranull.irondoors.interaction.InteractionHandler;
import com.ranull.irondoors.util.MaterialUtils;
import com.ranull.irondoors.util.NmsUtils;
import com.ranull.irondoors.util.VersionUtils;
import java.lang.reflect.Method;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.EquipmentSlot;

/**
 * Listener for PlayerInteractEvent
 *
 * @author Ranull
 */
public class PlayerInteractListener implements Listener {

    private static final boolean CAN_SWING_HAND = VersionUtils.v1_16_OR_NEWER;
    private static final boolean HAS_OFF_HAND = VersionUtils.v1_9_OR_NEWER;

    @SuppressWarnings({"deprecation", "unused"})
    @EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
    public void onPlayerInteract(final PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Block block = event.getClickedBlock();

        if (block != null
            && !player.isSneaking()
            && event.getAction() == Action.RIGHT_CLICK_BLOCK
            && ((block.getType().toString().equals(MaterialUtils.IRON_DOOR_NAME) && player
            .hasPermission("irondoors.irondoor"))
            || (block.getType().toString().equals(MaterialUtils.IRON_TRAPDOOR_NAME) && player
            .hasPermission("irondoors.irontrapdoor")))
        ) {
            if(HAS_OFF_HAND || event.getHand() != EquipmentSlot.HAND) return;

            //FIXME note to ranull: I've removed the 'is 1.7' check here since people should not
            //FIXME be running this on versions older than 1.7 anyways.

            if(!InteractionHandler.INSTANCE.canInteract(
                player, event.getPlayer().getItemInHand(), block, event.getBlockFace()
            )) return;

            InteractionHandler.INSTANCE.toggleDoor(block);
            swingMainHand(player);

            if(event.isBlockInHand()) {
                event.setCancelled(true);
            }
        }
    }

    public void swingMainHand(final Player player) {
        if (CAN_SWING_HAND) {
            player.swingMainHand();
        } else {
            try {
                Object entityPlayer = player.getClass().getMethod("getHandle").invoke(player);

                Object playerConnection = entityPlayer.getClass().getField("playerConnection")
                    .get(entityPlayer);

                Method sendPacket = playerConnection.getClass().getMethod("sendPacket",
                    NmsUtils.getNmsClass("Packet"));

                Object packetPlayOutAnimation = NmsUtils.getNmsClass("PacketPlayOutAnimation")
                    .getConstructor(NmsUtils.getNmsClass("Entity"), int.class)
                    .newInstance(entityPlayer, 0);

                sendPacket.invoke(playerConnection, packetPlayOutAnimation);
            } catch (final Exception ex) { throw new RuntimeException(ex); }
        }
    }
}
