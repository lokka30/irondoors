package com.ranull.irondoors.util;

import org.bukkit.Bukkit;

/**
 * Utilities for accessing Minecraft server internals.
 *
 * @author Ranull
 * @author lokka30
 */
public class NmsUtils {

    private NmsUtils() throws IllegalAccessException {
        throw new IllegalAccessException("Attempted instantiation of utility class");
    }

    /**
     * Retrieve the desired NMS class pertaining to the current server version.
     *
     * @param clazz desired NMS class
     * @return NMS class
     */
    public static Class<?> getNmsClass(String clazz) {
        try {
            return Class.forName("net.minecraft.server." + Bukkit.getServer().getClass()
                .getPackage().getName().replace(".", ",")
                .split(",")[3] + "." + clazz);
        } catch (final ClassNotFoundException ex) {
            throw new RuntimeException(ex);
        }
    }

}
