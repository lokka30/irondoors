package com.ranull.irondoors.util;

import java.util.Arrays;
import org.bukkit.Material;

/**
 * Utilities for using the correct material names on various server versions, i.e.,
 * before and after v1.13.
 *
 * @author lokka30
 */
public class MaterialUtils {

    private MaterialUtils() throws IllegalAccessException {
        throw new IllegalAccessException("Attempted instantiation of utility class");
    }

    public static final String IRON_DOOR_NAME =
        getAvailableMaterialName(
            /*
            WARNING: 'IRON_DOOR' exists in 1.7-1.12, but it is the item material, not the block
            material. Make sure that 'IRON_DOOR_BLOCK' is checked first to prevent the incorrect
            material name from being used in 1.7-1.12 servers.
             */
            "IRON_DOOR_BLOCK", //v1.7 - 1.12
            "IRON_DOOR" //v1.13+
        );

    public static final String IRON_TRAPDOOR_NAME =
        getAvailableMaterialName(
            "IRON_TRAPDOOR" //v1.8+
        );

    public static final String OAK_DOOR_NAME =
        getAvailableMaterialName(
            /*
            WARNING: 'IRON_DOOR' exists in 1.7-1.12, but it is the item material, not the block
            material. Make sure that 'IRON_DOOR_BLOCK' is checked first to prevent the incorrect
            material name from being used in 1.7-1.12 servers.
             */
            "WOODEN_DOOR", //v1.7 - 1.12
            "OAK_DOOR" //v1.13+
        );

    public static final String OAK_TRAPDOOR_NAME =
        getAvailableMaterialName(
            "TRAP_DOOR", //v1.7 - 1.12
            "OAK_TRAPDOOR" //v1.13+
        );

    // note: when calling this method, sort names by their versions (ascending).
    private static String getAvailableMaterialName(final String... possibleNames) {
        for(final String name : possibleNames) {
            try {
                Material.valueOf(name);
                return name;
            } catch (final IllegalArgumentException ignored) {}
        }

        throw new IllegalStateException("None of possible material names matched: " +
            Arrays.toString(possibleNames));
    }

}
