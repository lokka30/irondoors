package com.ranull.irondoors.util;

import org.bukkit.entity.EntityType;

/**
 * Utiltiies for checking which Minecraft version a server is running.
 *
 * @author lokka30
 */
public class VersionUtils {

    public static boolean v1_16_OR_NEWER = entityTypeExists("PIGLIN");
    public static boolean v1_13_OR_NEWER = entityTypeExists("DROWNED") || v1_16_OR_NEWER;
    public static boolean v1_10_OR_NEWER = entityTypeExists("STRAY") || v1_13_OR_NEWER;
    public static boolean v1_9_OR_NEWER = entityTypeExists("SHULKER") || v1_10_OR_NEWER;
    public static boolean v1_7_OR_NEWER = entityTypeExists("DARK_OAK_STAIRS") || v1_9_OR_NEWER;

    private VersionUtils() throws IllegalAccessException {
        throw new IllegalAccessException("Attempted instantiation of utility class");
    }

    private static boolean entityTypeExists(final String entityType) {
        try {
            EntityType.valueOf(entityType);
            return true;
        } catch (final IllegalArgumentException ignored) {
            return false;
        }
    }

}
